<?php
//  @copyright	Copyright (C) 2013 IceTheme. All Rights Reserved
//  @license	Copyrighted Commercial Software 
//  @author     IceTheme (icetheme.com)

defined('_JEXEC') or die;

// Include main PHP template file
include_once(JPATH_ROOT . "/templates/" . $this->template . '/php/template.php'); 

if ((JRequest::getCmd("tmpl", "index") != "offline")) { ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>

<?php if ($it_params_responsive == 1) { ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php } ?>

<jdoc:include type="head" />

	<?php
    // Include CSS and JS variables 
    include_once(IT_THEME_DIR.'/php/stylesheet.php');
    ?>
   
</head>

<body class="<?php echo htmlspecialchars($pageclass)?> <?php if ($it_mod_slideshow != 0) { ?>body_preload<?php } ?>"> 
	
	<div class="body_preload_area">
            
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
        
    </div>
	
	
    <header id="header" class="<?php if ($it_mod_slideshow == 0) { ?>no-slideshow<?php } ?> "> 
        	
        <div class="container"> 
        	
			<?php if ($it_params_logo != "") { ?>
			<div id="logo">	
			   <a href="<?php echo $this->baseurl ?>"><?php echo $it_params_logo_img; ?></a>        	
			</div>
			<?php } ?> 
	
			<?php if ($it_mod_categories != 0) { ?>
			<div id="vm-categories">
            
           		<a href="#" class="vm-categories_link"><span><?php echo JText::_('TPL_SHOP_DEPARTMENT'); ?></span></a>
				<jdoc:include type="modules" name="categories" /> 
                
			</div>
			<?php } ?>		
			
			<?php if ($it_mod_language != 0) { ?>
			<div id="language">
				<jdoc:include type="modules" name="language" /> 
			</div>
			<?php } ?>
			
			<?php if ($it_mod_contact != 0) { ?>
				<div id="header_contact">
                  	<jdoc:include type="modules" name="contact" />    
				</div>
            <?php } ?>
			
			<?php if (($it_mod_topmenu != 0)) { ?>
			<div id="topmenu">
				<jdoc:include type="modules" name="topmenu" />
			</div>
			<?php } ?>

			<?php if ($it_mod_search != 0) { ?>
			<div id="search">
				<jdoc:include type="modules" name="search" />
			</div>
			<?php } ?>
			  
			<div id="mainmenu" class="navbar <?php if ($it_mod_slideshow == 0) { ?>no-slideshow<?php } ?>">
					
				<div class="navbar-inner">
						
					<a class="btn btn-navbar" data-toggle="collapse" data-target="#mainmenu .nav-collapse">
					  <span class="icon-bar"></span>
					  <span class="icon-bar"></span>
					  <span class="icon-bar"></span>
					</a>
					
					<div class="nav-collapse collapse">
					
						<jdoc:include type="modules" name="mainmenu" />
						
						<?php if  ($it_mod_cart != 0){ ?> 
                        <div id="cart">
                            <jdoc:include type="modules" name="cart" /> 
                        </div>
						<?php } ?>
						  
					</div>     
					
				</div>

			</div>
			
		</div>
    
	</header>
    
	<?php if ($it_mod_slideshow != 0) { ?>
    <div id="slideshow">
        <jdoc:include type="modules" name="slideshow" />
    </div>
    
    <script type="text/javascript">
    jQuery(window).load (function () { 
        jQuery('body').removeClass('body_preload')
    });
    </script>
    <?php } ?> 
    
    <section id="content" class="<?php if ($it_mod_slideshow == 0) { ?>no-slideshow<?php } ?> ">
    
		<div class="container">
        
		    <?php if ($it_mod_breadcrumbs != 0) { ?>        
            <div id="breadcrumbs" class="<?php if ($it_mod_promo == 0) { ?>no-promo<?php } ?>">
                <jdoc:include type="modules" name="breadcrumbs" />
            </div>
            <?php } ?>
            
            <?php if ($it_mod_banner != 0) { ?> 
            <div id="banner_area">
                <div class="row-fluid">
                    <jdoc:include type="modules" name="banner" style="banner" />
                </div>
            </div>
            <?php } ?>
            
            <?php if ($it_mod_promo != 0 ) { ?> 
            <div id="promo">
                <div class="row-fluid">
                    <jdoc:include type="modules" name="promo" style="promo" />
                </div>
            </div>
            <?php } ?>  
            
            <?php if ($it_mod_message != 0 ) { ?> 
            <div id="message">
                <jdoc:include type="modules" name="message" style="xhtml" />
            </div>
            <?php } ?>  
            
            <?php if ($it_mod_product != 0) { ?>        
            <div id="vm_product">
                <jdoc:include type="modules" name="product" style="xhtml" />
            </div>
            <?php } ?>
            
           
            <?php if (!isset($it_hide_frontpage)) { ?> 
            <div id="content_inside">
                
                <div class="row-fluid">
                    
                    <div id="middlecol" class="<?php echo $it_content_span;?> <?php echo $it_sidebar_pos_class; ?>">
                    
                        <div class="inside"> 
                        
                            <jdoc:include type="message" />
                            <jdoc:include type="component" />

                        </div>
                        
                    </div>
                    
                    <?php if ($it_mod_sidebar != 0) { ?> 
                    <div id="sidebar" class="<?php echo $it_sidebar_span;?> <?php echo $it_sidebar_pos_class; ?>" >
                       
                        <div class="inside">  
                            
                            <jdoc:include type="modules" name="sidebar" style="sidebar" />
                            
                        </div>
                        
                    </div>
                    <?php } ?> 
                
                </div> 
               
            </div>
            <?php } ?>
            
            
			<?php if ($it_mod_brands != 0 ) { ?> 
            <div id="brands">
                <jdoc:include type="modules" name="brands" />
            </div>
            <script type="text/javascript">
            (function($) {
                $(function() { //on DOM ready 
                    $("#brands ul").simplyScroll();
                });
            })(jQuery);
            </script>
            <?php } ?>
            
            
    	</div> 
                
    </section><!-- / Content  --> 
	
    <?php if (($it_mod_showcase || $it_mod_testimonial) != 0) { ?>
    <section id="showcase" class="<?php if ($it_mod_testimonial == 0 ) { ?>no-testimonial<?php } ?> " style=" <?php echo $it_showcase_bg; ?>" data-stellar-background-ratio="0.5" data-stellar-vertical-offset="0">
        
        <div class="container">
        
            <div id="showcase_inner">
            
                <?php if ($it_mod_showcase != 0 ) { ?>
                <div id="showcase-modules"> 
                    <div class="row-fluid">
                        <jdoc:include type="modules" name="showcase" style="showcase" />
                    </div>
                </div>    
                <?php } ?> 
                
               	<?php if ($it_mod_testimonial != 0 ) { ?> 
                <div id="testimonials">
                    <jdoc:include type="modules" name="testimonial" />
                </div>   
                
                <script type="text/javascript">
                jQuery(document).ready(function() {
                    jQuery("#testimonials").owlCarousel({
                        navigation : true, // Show next and prev buttons
                        pagination : false,
                        slideSpeed : 700,
                        paginationSpeed : 700,
                        transitionStyle : "goDown", // "fade", "backSlide", goDown and fadeUp
                        singleItem:true
                        
                    })
                });
                </script>
                <?php } ?>   
      
                
            </div>
        
        </div>    
            
    </section>
    <script type="text/javascript">
    jQuery(document).ready(function() {
    jQuery.stellar({
        horizontalScrolling: false,
        responsive: true
        })
    });	
    </script>      
    <?php } ?> 
    
	
   
    <footer id="footer">

        <div class="container">
       
			<?php if ($it_mod_footer != 0) { ?>
            <div class="row-fluid">
                <jdoc:include type="modules" name="footer" style="footer" />
            </div>
            <?php } ?>
            
            <div id="copyright">
    
                <p class="copytext">
                     &copy; <?php echo date('Y');?> <?php echo $sitename. ""; ?> 
                     <?php // echo JText::_('TPL_RESERVED'); ?> 
                </p> 
                
                <jdoc:include type="modules" name="footermenu" />
                
            </div>
            
       </div>   
    
    </footer> 
        
         
         
    <?php if ($it_mod_loginModal != 0 ) { ?> 
    <!-- Login Modal -->
    <div id="loginModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel"><?php echo $modal_login_title; ?></h3>
      	</div>
        
        <div class="modal-body">
			<jdoc:include type="modules" name="modal_login" />
        </div>
        
    </div>
 	<?php } ?>  
    
	<?php if ($it_params_gotop != 0) { ?>
    <div id="gotop" class="">
        <a href="#" class="scrollup"><?php echo JText::_('TPL_SCROLL'); ?></a>
    </div>
    <?php } ?>  

        
	<?php 
		
		if ($it_mod_slide != 0) { 
			// Include slide module position
			include_once(IT_THEME_DIR.'/php/slide.php');
		}
		
		// if ($it_params_styleswitcher != 0) { 
			// Include style switcher JS 
			include_once(IT_THEME_DIR.'/php/style_switcher.php');
		// }
		
	?>
    
    <jdoc:include type="modules" name="debug" />
    
  
</body>
</html>
<?php } ?>