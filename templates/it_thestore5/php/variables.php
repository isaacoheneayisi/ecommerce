<?php
//  @copyright	Copyright (C) 2008 - 2014 IceTheme. All Rights Reserved
//  @license	Copyrighted Commercial Software 
//  @author     IceTheme (icetheme.com)

// No direct access.
defined('_JEXEC') or die;

// Template Parameters Variables
$templatestyle		 			 = $this->params->get('TemplateStyle');

$it_params_logo 					= $this->params->get('logo');
$it_params_logo_img				= '<img class="logo" src="'. JURI::root() . $this->params->get('logo') .'" alt="'. $sitename .'" />';
$it_params_styleswitcher		   = $this->params->get('styleswitcher');
$it_params_sidebar_pos    		 = $this->params->get('sidebar_position');
$it_params_sidebar_width		   = $this->params->get('sidebar_span');
$it_params_cuosom_css			  = $this->params->get('custom_css_code');
$it_params_gotop				   = $this->params->get('go2top');
$it_params_icelogo        	 	 = $this->params->get('icelogo');
$it_params_icelogo_title           = "We would like to inform that this website is designed by IceTheme.com with the latest standards provied by the World Wide Web Consortium (W3C)";
$it_params_hide_frontpage    	  = $this->params->get('hidefrontpage');

$it_params_offline_newsletter      = $this->params->get('offline_newsletter');
$it_params_offline_h2		      = $this->params->get('offline_h2');
$it_params_offline_count	       = $this->params->get('offline_count');
$it_params_offline_count_time      = $this->params->get('offline_count_time');
$it_params_offline_p		       = $this->params->get('offline_p');
$it_params_offline_social          = $this->params->get('offline_social');
$it_params_offline_form		    = $this->params->get('offline_form');

$it_params_responsive        	  = $this->params->get('responsive_template');
$it_params_advanced_bootstrap      = $this->params->get('advanced_bootstrap');
$it_params_advanced_icomoon      	= $this->params->get('advanced_icomoon');
$it_params_advanced_fontawesome    = $this->params->get('advanced_fontawesome');

$it_params_background_color       	= $this->params->get('backround_color');
$it_params_background_image		= $this->params->get('backround_image');

$it_params_background_position     = $this->params->get('backround_position');
$it_params_background_repeat       = $this->params->get('backround_repeat');

$it_params_equalcols     		   = $this->params->get('it_params_equalcols');
$it_params_welcome    		     = $this->params->get('it_params_welcome');


// Define Variables for module positions
$it_mod_usermenu				= $this->countModules('usermenu');
$it_mod_language				= $this->countModules('language');
$it_mod_topmenu  	  	     	 = $this->countModules('topmenu');
$it_mod_mainmenu				= $this->countModules('mainmenu');
$it_mod_search		          = $this->countModules('search');
$it_mod_slideshow			   = $this->countModules('slideshow');
$it_mod_breadcrumbs			 = $this->countModules('breadcrumbs');
$it_mod_icecarousel    		 = $this->countModules('icecarousel');
$it_mod_sidebar    			 = $this->countModules('sidebar');
$it_mod_copyrightmenu     	   = $this->countModules('copyrightmenu');
$it_mod_slide   	 			   = $this->countModules('slide');
$it_mod_loginModal  	 		  = $this->countModules('modal_login');

global $it_mod_promo; 			// we use this variable on modules.php functions
$it_mod_promo	 			   = $this->countModules('promo');
global $it_mod_showcase; 		 // we use this variable on modules.php functions
$it_mod_showcase	 			= $this->countModules('showcase');
global $it_mod_footer; 		   // we use this variable on modules.php functions
$it_mod_footer	 			  = $this->countModules('footer');
global $it_mod_banner; 		   // we use this variable on modules.php functions
$it_mod_banner	 			  = $this->countModules('banner');


// Only to this template
$it_mod_message		         = $this->countModules('message');
$it_mod_tagline	 		     = $this->countModules('tagline');
$it_mod_testimonial	 	     = $this->countModules('testimonial');
$it_mod_login	 		       = $this->countModules('login');
$it_mod_cart			        = $this->countModules('cart');
$it_mod_contact	 		     = $this->countModules('contact');
$it_mod_product	 		     = $this->countModules('product');
$it_mod_categories	 		  = $this->countModules('categories');
$it_mod_brands	 		  	  = $this->countModules('brands');


$it_showcase_bg_param		= $this->params->get('showcase_bg');
if ($it_showcase_bg_param != "") {
	$it_showcase_bg			= "background-image:url(".$it_showcase_bg_param .");";
}

// Header background image
$it_params_header_background_image		= $this->params->get('header_backround_image');


/* get the login module title */
if ($it_mod_loginModal != 0 ) {
$get_login_module = JModuleHelper::getModule( 'login');
$modal_login_title = $get_login_module->title;
}

/* get slide module */
$modules_slider = JModuleHelper::getModules( 'slider' );

?>