<?php
//  @copyright	Copyright (C) 2008 - 2014 IceTheme. All Rights Reserved
//  @license	Copyrighted Commercial Software 
//  @author     IceTheme (icetheme.com)

// No direct access.
defined('_JEXEC') or die;
?>

<?php 

// load complimentary css files
$document->addStyleSheet(IT_THEME. '/assets/css/owl.carousel.css');

// Load main Template CSS
$document->addStyleSheet(IT_THEME. '/assets/less/template.css');

if($it_params_responsive == 1) {
	$document->addStyleSheet(IT_THEME. '/assets/less/template_responsive.css');
}
	
?>

<?php if($it_params_advanced_fontawesome == 1) {  ?>
<link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" >
<?php } ?>

<link id="stylesheet" rel="stylesheet" type="text/css" href="<?php echo IT_THEME; ?>/assets/less/styles/<?php echo $templatestyle; ?>.css" />

<link rel="stylesheet" type="text/css" href="<?php echo IT_THEME; ?>/assets/css/custom.css" />

<style type="text/css" media="screen">

body {
	
	<?php if($it_params_background_image != "default") {  ?>
	background-image: url("<?php echo  IT_THEME ."/assets/images/styles/". $it_params_background_image ."/bg_pattern.png"; ?>");
	<?php } ?>
	
	<?php if($it_params_background_image == "none") {  ?>
	background-image:none;
	<?php } ?>
}	


<?php if ( ($it_params_equalcols == 1) && ($it_mod_sidebar != 0) ) {  ?>	
/* Equal Height Columns */
#content #content_inside > .row-fluid {
	overflow: hidden; 
}

#content #middlecol,
#content #sidebar {
	margin-bottom: -99999px!important;
	padding-bottom: 100014px!important;
}
<?php } ?>

/* Custom CSS code through template paramters */
<?php echo $it_params_cuosom_css; ?>

</style>


<!-- Google Fonts -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300|Coming+Soon' rel='stylesheet' type='text/css' />


<!--[if lte IE 9]>
	<link rel="stylesheet" type="text/css" href="<?php echo IT_THEME; ?>/assets/css/ie9.css" />
<![endif]-->

<!--[if lte IE 8]>
    <link rel="stylesheet" type="text/css" href="<?php echo IT_THEME; ?>/assets/css/ie8.css" />
    <script src="<?php echo $this->baseurl ?>/media/jui/js/html5.js"></script>
    <script src="<?php echo IT_THEME; ?>/assets/js/respond.min.js"></script>
<![endif]-->

<!--[if !IE]><!-->
<script>  
if(Function('/*@cc_on return document.documentMode===10@*/')()){
    document.documentElement.className+=' ie10';
}
var b = document.documentElement;
b.setAttribute('data-useragent', navigator.userAgent);
b.setAttribute('data-platform', navigator.platform);
</script>
<!--<![endif]-->  

<style type="text/css">

/* IE10 hacks. add .ie10 before */
.ie10 ul#ice-switcher,
html[data-useragent*='rv:11.0'] ul#ice-switcher {
	padding-right:20px;}  
	.ie10 ul#ice-switcher:hover,
	html[data-useragent*='rv:11.0'] ul#ice-switcher:hover {
		padding-right:35px}

.ie10 ul#ice-switcher li.active a,
.ie10 ul#ice-switcher li a:hover{
	padding-top:0;
	padding-bottom:0}
					
.ie10 ul#ice-switcher li a span,
html[data-useragent*='rv:11.0'] ul#ice-switcher li a span {
	padding-left:30px;}
	
.ie10 #gotop .scrollup,
html[data-useragent*='rv:11.0'] #gotop .scrollup {
	right:40px;}

</style>




