/***********************************************************************************************/
/* Main IceTheme template Scripts */
/***********************************************************************************************/


/* default joomla script */
(function($)
{
	$(document).ready(function()
	{
		$('*[rel=tooltip]').tooltip()

		// Turn radios into btn-group
		$('.radio.btn-group label').addClass('btn');
		$(".btn-group label:not(.active)").click(function()
		{
			var label = $(this);
			var input = $('#' + label.attr('for'));

			if (!input.prop('checked')) {
				label.closest('.btn-group').find("label").removeClass('active btn-success btn-danger btn-primary');
				if (input.val() == '') {
					label.addClass('active btn-primary');
				} else if (input.val() == 0) {
					label.addClass('active btn-danger');
				} else {
					label.addClass('active btn-success');
				}
				input.prop('checked', true);
			}
		});
		$(".btn-group input[checked=checked]").each(function()
		{
			if ($(this).val() == '') {
				$("label[for=" + $(this).attr('id') + "]").addClass('active btn-primary');
			} else if ($(this).val() == 0) {
				$("label[for=" + $(this).attr('id') + "]").addClass('active btn-danger');
			} else {
				$("label[for=" + $(this).attr('id') + "]").addClass('active btn-success');
			}
		});
	})
})(jQuery);


/* Responsive table (pure JS */
jQuery(window).on('load', function() {
	// Check all tables. You may need to be more restrictive.
	jQuery('table').each(function() {
		var element = jQuery(this);
		// Create the wrapper element
		var scrollWrapper = jQuery('<div />', {
			'class': 'scrollable',
			'html': '<div />' // The inner div is needed for styling
		}).insertBefore(element);
		// Store a reference to the wrapper element
		element.data('scrollWrapper', scrollWrapper);
		// Move the scrollable element inside the wrapper element
		element.appendTo(scrollWrapper.find('div'));
		// Check if the element is wider than its parent and thus needs to be scrollable
		if (element.outerWidth() > element.parent().outerWidth()) {
			element.data('scrollWrapper').addClass('has-scroll');
		}
		// When the viewport size is changed, check again if the element needs to be scrollable
		jQuery(window).on('resize orientationchange', function() {
			if (element.outerWidth() > element.parent().outerWidth()) {
				element.data('scrollWrapper').addClass('has-scroll');
			} else {
				element.data('scrollWrapper').removeClass('has-scroll');
			}
		});
	});
});


/* jQuery scripts for IceTheme template */
jQuery(document).ready(function() { 
	
	/* detect if broser is ipad */
	var iPad_detect = navigator.userAgent.match(/iPad/i) != null;
	if (iPad_detect) {
		jQuery("body").addClass("ipad");  
	}
	 

	/*  Go to Top Link 
	-------------------*/
	 jQuery(window).scroll(function(){
		if ( jQuery(this).scrollTop() > 1000) {
			 jQuery("#gotop").addClass("gotop_active");
		} else {
			 jQuery("#gotop").removeClass("gotop_active");
		}
	}); 
	jQuery(".scrollup").click(function(){
		jQuery("html, body").animate({ scrollTop: 0 }, 600);
		return false;
	});

	
	/* initialize bootstrap tooltips */
	jQuery("[rel='tooltip']").tooltip();
	
	/* initialize bootstrap popopver */
	jQuery("[rel='popover']").popover();
	
	
	
	/* Languages Module 
	-------------------*/
	/* language module hover efffect for flags */
	jQuery(".mod-languages").hover(function () {
		jQuery(".mod-languages li").css({opacity : .25});
	  }, 
	  function () {
		jQuery(".mod-languages li").css({ opacity : 1});
	  }
	);	
	/* language module remove title attribute from img*/
	jQuery("div.mod-languages img").data("title", jQuery(this).attr("title")).removeAttr("title");
	
	
	/* Brands logos */
	jQuery("#brands").hover(function () {
		jQuery("#brands li").addClass("brands_hover"); 
	  }, 
	  function () {
		jQuery("#brands li").removeClass("brands_hover"); 
	  }
	);	

	/* small plugin to do an event outside the element */
	(function($){
    $.fn.outside = function(ename, cb){
        return this.each(function(){
            var $this = $(this),
                self = this;

            $(document).bind(ename, function tempo(e){
                if(e.target !== self && !$.contains(self, e.target)){
                    cb.apply(self, [e]);
                    if(!self.parentNode) $(document.body).unbind(ename, tempo);
                }
            });
        });
    };
	}(jQuery));

		
	/* Search 
	---------*/
	/* adjust the search module */
    jQuery("#search form .inputbox").attr('maxlength','35');
	jQuery("#search form .btn").addClass("icebtn"); 
	


	/* Joomla Rating 
	-----------------*/
	/* fix some classes on the content voting */
	jQuery("span.content_vote .btn").removeClass("btn-mini");
	
	
	/* BreadCrumbs  
	--------------*/
	/* fix breadcrumbs */
	jQuery("ul.breadcrumb .divider.icon-location").removeClass("divider icon-location");
	
	
	/* Joomla Articles  
	------------------*/
	/* remove links from article titles */
	jQuery(".item-page .page-header h2 a").contents().unwrap();
	
	
	/* Joomla Newsflas module 
	-------------------------*/ 
	/* only in carousel mode remove introtext but leave the img */
	jQuery('.newsflash-carousel-item p:first-child').contents().filter(function(){ 
		return this.nodeType === 3; 
	}).remove();
	
	
	
	/* login modal box
	------------------*/
	jQuery('.loginModal').click(function(){
		jQuery('#loginModal').modal('show');
	});
	

	/* Main menu 
	---------------*/
	jQuery('.dropdown-toggle').click(function () {
		jQuery('#slideshow').toggleClass('icemegamenu-hover');
	});
	
	jQuery('.dropdown-toggle').outside('click', function() {
		jQuery('#slideshow').removeClass('icemegamenu-hover');
	});


	/* Slideshow module (CK slideshow) 
	----------------------------------*/
	jQuery(".camera_caption_title, .camera_caption_desc").addClass("container");
	
	
	/* Footer menu
	---------------
	jQuery("#copyright ul.menu").hover(function () {
		jQuery("#copyright ul.menu li a").css({opacity : .6});
	  }, 
	  function () {
		jQuery("#copyright ul.menu a").css({ opacity : 1});
	  }
	);*/
	
	
	
	/* Virtuemart 
	--------------*/
	jQuery('a.vm-categories_link').click(function () {
		jQuery('a.vm-categories_link').toggleClass('vm-categories_link_open');
		jQuery('#vm-categories > ul.VMmenu').toggleClass('vm-categories_expanded');
		jQuery('#slideshow').toggleClass('icemegamenu-hover');
	});
	
	jQuery('#vm-categories').outside('click', function() {
		jQuery('a.vm-categories_link').removeClass('vm-categories_link_open');
		jQuery('#vm-categories > ul.VMmenu').removeClass('vm-categories_expanded');
		jQuery('#slideshow').removeClass('icemegamenu-hover');
	});
	
	jQuery('.button, .default').addClass('btn');
	
	jQuery('#cart').click(function () {
		jQuery("#header").addClass('header_fix_cart');
		jQuery("#slideshow").addClass('header_fix_cart');
	});
	
	jQuery('#cart').outside('click', function() {
		jQuery("#header").removeClass('header_fix_cart');
		jQuery("#slideshow").removeClass('header_fix_cart');
	});

});  


	
// detect if screen is with touch or not (pure JS)
if (("ontouchstart" in document.documentElement)) {
	document.documentElement.className += "with-touch";
}else {
	document.documentElement.className += "no-touch";
}



					
