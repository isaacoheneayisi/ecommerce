<?php // no direct access
defined('_JEXEC') or die('Restricted access');

//dump ($cart,'mod cart');
// Ajax is displayed in vm_cart_products
// ALL THE DISPLAY IS Done by Ajax using "hiddencontainer" ?>


 <!-- Virtuemart 2 Ajax Card -->
<div id="vmCartModule" class="vmCartModule <?php echo $params->get('moduleclass_sfx'); ?>">
        
    <a href="#cartModal" data-toggle="modal" class="cart_modal_link"><span class="total_products"><?php echo  $data->totalProductTxt ?></span></a>
   
    <!-- Modal -->
    <div id="cartModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel"><?php echo vmText::_('COM_VIRTUEMART_CART_OVERVIEW') ?></h3>
      </div>
      
      <div class="modal-body">
        
		<?php
        if ($show_product_list) {
            ?>
            
            <div id="hiddencontainer" style=" display: none; ">
                <div class="vmcontainer">
                    <div class="product_row">
                        <span class="quantity"></span>&nbsp;x&nbsp;<span class="product_name"></span>
        
                    <?php if ($show_price and $currencyDisplay->_priceConfig['salesPrice'][0]) { ?>
                        <div class="subtotal_with_tax" style="float: right;"></div>
                    <?php } ?>
                    <div class="customProductData"></div>
                    </div>
                </div>
            </div>
            
            <div class="vm_cart_products">
            
                <div class="vmcontainer">
        
                <?php
                    foreach ($data->products as $product){
                        ?>
                      <div class="product_row">
                            
                            <span class="quantity"><?php echo  $product['quantity'] ?></span>&nbsp;x&nbsp;<span class="product_name"><?php echo  $product['product_name'] ?></span>
                            
                        <?php if ($show_price and $currencyDisplay->_priceConfig['salesPrice'][0]) { ?>
                          <div class="subtotal_with_tax" style="float: right;"><?php echo $product['subtotal_with_tax'] ?></div>
                        <?php } ?>
                        
                        <?php if ( !empty($product['customProductData']) ) { ?>
                            <div class="customProductData"><?php echo $product['customProductData'] ?></div>
                        <?php } ?>
        
                    </div>
                    
                <?php }
                ?>
                
                </div>
                
            </div>
            <?php } ?>
            
            <hr />
        
            <div class="total">
                <?php if ($data->totalProduct and $show_price and $currencyDisplay->_priceConfig['salesPrice'][0]) { ?>
                <?php echo $data->billTotal; ?>
                <?php } ?>
            </div>
        
            <div class="total_products"><?php echo  $data->totalProductTxt ?></div>
            
            <div style="clear:both;"></div>
            
            <div class="payments_signin_button" ></div>
            
            <noscript>
            <?php echo vmText::_('MOD_VIRTUEMART_CART_AJAX_CART_PLZ_JAVASCRIPT') ?>
            </noscript>
            
        </div>
    
        
        <div class="modal-footer">
        <button style="display:none" class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
         <div class="show_cart">
			<?php if ($data->totalProduct) echo $data->cart_show; ?>
        </div>
        </div>
    
    </div>
    
</div>