<?php // no direct access
defined ('_JEXEC') or die('Restricted access');
// add javascript for price and cart, need even for quantity buttons, so we need it almost anywhere
vmJsApi::jPrice();

?>



	<?php if ($headerText) { ?>
	<div class="vmheader"><?php echo $headerText ?></div>
	<?php } ?>
    
    <div id="vm_product-<?php echo $module->id;?>" class="vmgroup<?php echo $params->get ('moduleclass_sfx') ?>">

	
		<?php foreach ($products as $product) { ?>
        
            <div class="vmproduct<?php echo $params->get ('moduleclass_sfx'); ?> productdetails">
               	
                <?php
					if (!empty($product->images[0])) {
						$image = $product->images[0]->displayMediaThumb ('class="featuredProductImage" border="0"', FALSE);
					} else {
						$image = '';
					}
					echo JHTML::_ ('link', JRoute::_ ('index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $product->virtuemart_product_id . '&virtuemart_category_id=' . $product->virtuemart_category_id), $image, array('title' => $product->product_name));
					echo '<div class="clear"></div>';
					$url = JRoute::_ ('index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $product->virtuemart_product_id . '&virtuemart_category_id=' .
						$product->virtuemart_category_id); ?>
					<a href="<?php echo $url ?>"><?php echo $product->product_name ?></a>        <?php    echo '<div class="clear"></div>';

					if ($show_price) {
						// 		echo $currency->priceDisplay($product->prices['salesPrice']);
						if (!empty($product->prices['salesPrice'])) {
							echo $currency->createPriceDiv ('salesPrice', '', $product->prices, FALSE, FALSE, 1.0, TRUE);
						}
						// 		if ($product->prices['salesPriceWithDiscount']>0) echo $currency->priceDisplay($product->prices['salesPriceWithDiscount']);
						if (!empty($product->prices['salesPriceWithDiscount'])) {
							echo $currency->createPriceDiv ('salesPriceWithDiscount', '', $product->prices, FALSE, FALSE, 1.0, TRUE);
						}
					}
					if ($show_addtocart) {
						echo mod_ice_virtuemart_carousel::addtocart ($product);
					}
					?>
                    
            </div>
    
        <?php } ?>
    
    
		<script type="text/javascript">
        jQuery(document).ready(function() {
            jQuery("#vm_product-<?php echo $module->id;?>").owlCarousel({
				
				 // Most important owl features
				items : <?php echo $it_params_nr_columns ?>,
				
				//Basic Speeds
				slideSpeed : <?php echo $it_params_slide_speed ?>,
				paginationSpeed : <?php echo $it_params_slide_speed ?>,
				rewindSpeed : <?php echo $it_params_rewind_speed ?>,
			 
				//Autoplay
				autoPlay : <?php if($it_params_autoplay == 0) { echo "false"; } elseif ($it_params_autoplay == 5000) { echo "5000"; } else { echo "10000"; } ?>,
				stopOnHover : true,
			 
				// Navigation
				navigation : <?php if($it_params_navigation == 0) { echo "false"; } else { echo "true"; } ?>,
				navigationText : ["prev","next"],
				rewindNav : true,
				scrollPerPage : true,
			 
				//Pagination
				pagination : <?php if($it_params_pagination == 0) { echo "false"; } else { echo "true"; } ?>,
				paginationNumbers: false,
				
			})
		});
        </script>
       
	
	</div>
	
	<?php if ($footerText) : ?>
    <div class="vmfooter<?php echo $params->get ('moduleclass_sfx') ?>">
        <?php echo $footerText ?>
    </div>
    <?php endif; ?>